Name:           perl-Net-DNS-Resolver-Mock
Version:        1.20230216
Release:        2
Summary:        A subclass for mocking a DNS Resolver object
License:        GPL-1.0-or-later or Artistic-1.0
URL:            https://metacpan.org/release/Net-DNS-Resolver-Mock
Source0:        https://cpan.metacpan.org/authors/id/M/MB/MBRADSHAW/Net-DNS-Resolver-Mock-%{version}.tar.gz
BuildArch:      noarch

BuildRequires:  make perl-generators perl-interpreter perl >= 0:5.006
BuildRequires:  perl(ExtUtils::MakeMaker) >= 6.76 perl(strict) perl(warnings)
BuildRequires:  perl(Net::DNS::Packet) perl(Net::DNS::Question)
BuildRequires:  perl(Net::DNS::Resolver) perl(Net::DNS::ZoneFile) perl(Test::More) 
BuildRequires:  perl(Test::Exception)


%description
The perl-Net-DNS-Resolver-Mock package contains a subclass of
Net::DNS::Resolver which mocks a DNS Resolver object primarily used
for testing.

%package help
Summary:        Documents for perl-Net-DNS-Resolver-Mock

%description help
The perl-Net-DNS-Resolver-Mock-help package contains related documents.

%prep
%autosetup -n Net-DNS-Resolver-Mock-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1
%make_build

%install
make pure_install DESTDIR=%{buildroot}
%{_fixperms} %{buildroot}/*

%check
make test

%files
%license LICENSE
%doc README
%{perl_vendorlib}/*

%files help
%{_mandir}/man3/*

%changelog
* Sun Jan 19 2025 Funda Wang <fundawang@yeah.net> - 1.20230216-2
- drop useless perl(:MODULE_COMPAT) requirement

* Mon Sep 11 2023 yaoxin <yao_xin001@hoperun.com> - 1.20230216-1
- Update to 1.20230216

* Tue Jun 21 2022 SimpleUpdate Robot <tc@openeuler.org> - 1.20200215-1
- Upgrade to version 1.20200215

* Fri Feb 21 2020 Jiangping Hu <hujp1985@foxmail.com> - 1.20171219-5
- Package init
